var trusted = [];

var banned = process.env.BANNED_IPS ? process.env.BANNED_IPS.split(',') : [];

module.exports = {
	trusted:  trusted,
	banned:   banned,
	reserved: []
};
