FROM node:lts-alpine AS builder
ARG GRUNT_TASK=default
WORKDIR /qstats-server
COPY ["package.json", "package-lock.json*", "./"]
RUN npm ci --only=production && npm install -g grunt-cli
COPY --chown=node:node . .
RUN grunt $GRUNT_TASK

FROM node:lts-alpine
RUN apk add dumb-init
WORKDIR /qstats-server
COPY --chown=node:node --from=builder /qstats-server .
USER node
EXPOSE  3000
CMD ["dumb-init", "node", "./bin/www"]
